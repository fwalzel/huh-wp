<?php
/**
 * Plugin Name: Meta Description
 *
 * Description: Adds a meta description to header
 * Version: 1.0.0
 * Package: _metadesc
 * Author: Florian Walzel
 * License: MIT License
 */


function _metadesc_header_metadata() {

    if ( is_home() OR is_front_page() ) {
        ?>

        <meta name="description" content="Das spezielle Boutique-Hotel in München: für alle, die für Übernachtung, Konferenz, Tagungsräume, Seminarräume oder Event das Besondere suchen">

        <?php
    }
}
add_action( 'wp_head', '_metadesc_header_metadata' );